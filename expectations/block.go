package expectations

import (
	"net/http"
	"productoffers/common/integration/assertions"

	ginkgo "github.com/onsi/ginkgo"
)

// Block ...
type Block struct {
	description string
	assertables []*assertions.Assertable
}

// NewBlock ...
func NewBlock(description string, assertables []*assertions.Assertable) *Block {
	section := new(Block)
	section.description = description
	section.assertables = assertables
	return section
}

// RunAssertions ...
func (section *Block) RunAssertions(response *http.Response) bool {
	ok := true

	ginkgo.Describe(section.description, func() {
		for _, assertable := range section.assertables {

			fn := func(assertable *assertions.Assertable) func() {
				return func() {
					if assertOK := assertable.Assert(response); !assertOK {
						ok = false
					}
				}
			}

			ginkgo.It(assertable.GetDescription(), fn(assertable))

		}
	})

	return ok
}
