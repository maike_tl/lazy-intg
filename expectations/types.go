package expectations

// ISpecAssertion ...
type ISpecAssertion interface {
	GetMatcher() string
	GetValue() interface{}
}

// ISpecExpectation ...
type ISpecExpectation interface {
	GetStatus() int
	GetHeaders() map[string]ISpecAssertion
	GetCookies() map[string]ISpecAssertion
	GetBody() ISpecAssertion
}
