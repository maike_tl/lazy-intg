package expectations

import (
	"io/ioutil"
	"net/http"
	"productoffers/common/integration/assertions"
	"productoffers/common/integration/matchers"
)

// Expectation ...
type Expectation struct {
	spec   ISpecExpectation
	blocks []*Block
}

// NewExpectation ...
func NewExpectation(e ISpecExpectation) (*Expectation, error) {
	expectation := new(Expectation)
	expectation.spec = e
	expectation.blocks = make([]*Block, 0)

	if err := expectation.addStatusSection(); err != nil {
		return nil, err
	}

	if err := expectation.addHeadersSection(); err != nil {
		return nil, err
	}

	if err := expectation.addCookiesSection(); err != nil {
		return nil, err
	}

	if err := expectation.addBodySection(); err != nil {
		return nil, err
	}

	return expectation, nil
}

// Assert ...
func (expectation *Expectation) Assert(response *http.Response) bool {
	ok := true

	for _, block := range expectation.blocks {
		if sOK := block.RunAssertions(response); !sOK {
			ok = false
		}
	}

	return ok
}

func (expectation *Expectation) addStatusSection() error {
	fn := func(response *http.Response) interface{} {
		return response.StatusCode
	}

	if matcher, err := matchers.New("equal", expectation.spec.GetStatus()); err == nil {
		assertable := assertions.NewAssertable("status code", matcher, fn)

		block := NewBlock("when checking returned status code", []*assertions.Assertable{assertable})

		expectation.blocks = append(expectation.blocks, block)
	} else {
		return err
	}

	return nil
}

func (expectation *Expectation) addHeadersSection() error {
	headers := expectation.spec.GetHeaders()
	assertables := make([]*assertions.Assertable, len(headers))
	i := 0

	for headerName, header := range headers {
		if matcher, err := matchers.New(header.GetMatcher(), header.GetValue()); err == nil {
			assertables[i] = assertions.NewAssertable("header '"+headerName+"'", matcher, getHeaderValue(headerName))
			i++
		} else {
			return err
		}
	}

	block := NewBlock("when checking returned headers", assertables)
	expectation.blocks = append(expectation.blocks, block)

	return nil
}

func (expectation *Expectation) addCookiesSection() error {
	cookies := expectation.spec.GetCookies()
	assertables := make([]*assertions.Assertable, len(cookies))
	i := 0

	for cookieName, cookie := range cookies {
		if matcher, err := matchers.New(cookie.GetMatcher(), cookie.GetValue()); err == nil {
			assertables[i] = assertions.NewAssertable("cookie '"+cookieName+"'", matcher, getCookieValue(cookieName))
			i++
		} else {
			return err
		}
	}

	block := NewBlock("when checking returned cookies", assertables)
	expectation.blocks = append(expectation.blocks, block)

	return nil
}

func (expectation *Expectation) addBodySection() error {
	fn := func(response *http.Response) interface{} {
		defer response.Body.Close()

		data, _ := ioutil.ReadAll(response.Body)

		return data
	}

	bodySpec := expectation.spec.GetBody()

	if bodySpec.GetMatcher() == "" {
		return nil
	}

	if matcher, err := matchers.New(bodySpec.GetMatcher(), bodySpec.GetValue()); err == nil {
		assertable := assertions.NewAssertable("body", matcher, fn)

		block := NewBlock("when checking returned body", []*assertions.Assertable{assertable})

		expectation.blocks = append(expectation.blocks, block)
	} else {
		return err
	}

	return nil
}

func getHeaderValue(name string) assertions.AssertableValueGetter {
	return func(response *http.Response) interface{} {
		v := response.Header.Get(name)

		if v == "" {
			return nil
		}

		return v
	}
}

func getCookieValue(name string) assertions.AssertableValueGetter {
	return func(response *http.Response) interface{} {
		for _, cookie := range response.Cookies() {
			if cookie.Name == name {
				return cookie.Value
			}
		}

		return nil
	}
}
