package integration

import (
	"fmt"
	"productoffers/common/integration/expectations"
)

// SpecRunner ...
type SpecRunner struct {
	setup       setupFn
	teardown    teardownFn
	description string
	scenario    []*ScenarioRequest
	request     *Request
	expectation *expectations.Expectation
}

type setupFn func() error
type teardownFn func() error

// NewSpecRunner ...
func NewSpecRunner(spec Spec, setup ISpecSetup, teardown ISpecTearDown) (*SpecRunner, error) {
	r := new(SpecRunner)

	if setup != nil {
		r.setup = setup.Setup
	}

	if teardown != nil {
		r.teardown = teardown.TearDown
	}

	r.description = spec.Description

	r.setScenario(spec.Host, spec.Scenario)
	r.setRequest(spec.Host, spec.Request)

	if err := r.setExpectation(spec.Expected); err != nil {
		return nil, err
	}

	return r, nil
}

func (r *SpecRunner) setScenario(host string, scenario []SpecScenarioRequest) {
	r.scenario = make([]*ScenarioRequest, len(scenario))

	for i, req := range scenario {
		r.scenario[i] = NewScenarioRequest(host, req)
	}
}

func (r *SpecRunner) setRequest(host string, req SpecRequest) {
	r.request = NewRequest(host, req)
}

func (r *SpecRunner) setExpectation(expectation SpecExpectation) error {
	if expectation, err := expectations.NewExpectation(&expectation); err == nil {
		r.expectation = expectation
	} else {
		return fmt.Errorf("error preparing expectation: %s", err)
	}

	return nil
}

func (r *SpecRunner) run() error {
	if r.setup != nil {
		if err := r.setup(); err != nil {
			return fmt.Errorf("%s: error during test fixture setup: %s", r.description, err)
		}
	}

	if err := r.prepareScenario(); err != nil {
		return fmt.Errorf("%s: error preparing scenario: %s", r.description, err)
	}

	if err := r.assert(); err != nil {
		return fmt.Errorf("%s: error running spec: %s", r.description, err)
	}

	if r.teardown != nil {
		if err := r.teardown(); err != nil {
			return fmt.Errorf("%s: error during test fixture teardown: %s", r.description, err)
		}
	}

	return nil
}

func (r *SpecRunner) prepareScenario() error {
	for _, req := range r.scenario {
		res, err := req.Do()

		if err != nil {
			return fmt.Errorf("Error on request '%s': %s", req.description, err)
		}

		if !req.isValidResponseStatus(res.StatusCode) {
			return fmt.Errorf("Error  on request '%s'. It returned status code: %d", req.description, res.StatusCode)
		}
	}

	return nil
}

func (r *SpecRunner) assert() error {
	res, err := r.request.Do()

	if err != nil {
		return fmt.Errorf("Error on request: %s", err)
	}

	r.expectation.Assert(res)

	return nil
}
