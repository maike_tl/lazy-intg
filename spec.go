package integration

import (
	_ "productoffers/common/integration/matchers/defaults" // cause yes
)

// RunSpec ...
func RunSpec(spec Spec, setup ISpecSetup, teardown ISpecTearDown) error {
	return runSpec(spec, setup, teardown)
}

// RunSpecFromFixtureFile ...
func RunSpecFromFixtureFile(path string, setup ISpecSetup, teardown ISpecTearDown) error {
	var spec Spec

	if err := loadFixture(path, &spec); err != nil {
		return err
	}

	return runSpec(spec, setup, teardown)
}

func runSpec(spec Spec, setup ISpecSetup, teardown ISpecTearDown) error {
	if r, err := NewSpecRunner(spec, setup, teardown); err != nil {
		return err
	} else if err := r.run(); err != nil {
		return err
	}

	return nil
}
