package integration

import (
	"encoding/json"
	"productoffers/common/integration/expectations"
)

// Spec represents a use case to be tested.
type Spec struct {
	Description string                `json:"description"`
	Host        string                `json:"host"`
	Scenario    []SpecScenarioRequest `json:"scenario"`
	Request     SpecRequest           `json:"request"`
	Expected    SpecExpectation       `json:"expected"`
}

// SpecScenarioRequest ...
type SpecScenarioRequest struct {
	Description string `json:"description"`
	SpecRequest
}

// SpecRequest ...
type SpecRequest struct {
	Method  string            `json:"method"`
	URI     string            `json:"uri"`
	Cookies map[string]string `json:"cookies"`
	Headers map[string]string `json:"headers"`
	Body    json.RawMessage   `json:"body"`
}

// SpecExpectation ...
type SpecExpectation struct {
	Status  int                      `json:"status"`
	Cookies map[string]SpecAssertion `json:"cookies"`
	Headers map[string]SpecAssertion `json:"headers"`
	Body    SpecAssertion            `json:"body"`
}

// GetStatus ...
func (exp *SpecExpectation) GetStatus() int {
	return exp.Status
}

// GetHeaders ...
func (exp *SpecExpectation) GetHeaders() map[string]expectations.ISpecAssertion {
	assertions := make(map[string]expectations.ISpecAssertion)

	for key, value := range exp.Headers {
		assertions[key] = expectations.ISpecAssertion(value)
	}

	return assertions
}

// GetCookies ...
func (exp *SpecExpectation) GetCookies() map[string]expectations.ISpecAssertion {
	assertions := make(map[string]expectations.ISpecAssertion)

	for key, value := range exp.Cookies {
		assertions[key] = expectations.ISpecAssertion(value)
	}

	return assertions
}

// GetBody ...
func (exp *SpecExpectation) GetBody() expectations.ISpecAssertion {
	return expectations.ISpecAssertion(exp.Body)
}

// SpecAssertion ...
type SpecAssertion struct {
	Matcher string
	Value   interface{}
}

// GetMatcher ...
func (assertion SpecAssertion) GetMatcher() string {
	return assertion.Matcher
}

// GetValue ...
func (assertion SpecAssertion) GetValue() interface{} {
	return assertion.Value
}

// UnmarshalJSON ...
func (assertion *SpecAssertion) UnmarshalJSON(b []byte) error {
	var fields map[string]json.RawMessage

	if err := json.Unmarshal(b, &fields); err != nil {
		return err
	}

	json.Unmarshal(fields["matcher"], &assertion.Matcher)

	if value, ok := fields["value"]; ok {
		// TODO cast to boolean, string, int or float
		if err := json.Unmarshal(value, &assertion.Value); err != nil {
			return err
		}
	} else {
		assertion.Value = nil
	}

	return nil
}

// ISpecSetup defines the contract for a type that can setup a test
type ISpecSetup interface {
	Setup() error
}

// ISpecTearDown defines the contract for a type that can teardown a test
type ISpecTearDown interface {
	TearDown() error
}
