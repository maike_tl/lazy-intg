package integration

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

func loadFixture(path string, dest *Spec) error {
	file, err := os.Open(path)

	if err != nil {
		return err
	}

	data, err := ioutil.ReadAll(file)

	if err != nil {
		return err
	}

	if err := json.Unmarshal(data, dest); err != nil {
		return err
	}

	return nil
}
