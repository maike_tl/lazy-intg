package assertions

import (
	"net/http"
	"productoffers/common/integration/matchers"
)

// AssertableValueGetter ...
type AssertableValueGetter func(*http.Response) interface{}

// Assertable ...
type Assertable struct {
	description string
	matcher     matchers.IMatcher
	getter      AssertableValueGetter
}

// NewAssertable ...
func NewAssertable(
	description string,
	matcher matchers.IMatcher,
	getter AssertableValueGetter,
) *Assertable {
	assertable := new(Assertable)
	assertable.description = description
	assertable.matcher = matcher
	assertable.getter = getter
	return assertable
}

// Assert ...
func (assertable *Assertable) Assert(response *http.Response) bool {
	return assertable.matcher.Match(assertable.getter(response))
}

// GetDescription ...
func (assertable *Assertable) GetDescription() string {
	return assertable.description + " should " + assertable.matcher.String()
}
