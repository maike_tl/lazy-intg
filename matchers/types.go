package matchers

// IMatcherFactory ...
type IMatcherFactory func(...interface{}) IMatcher

// IMatcher ...
type IMatcher interface {
	Match(...interface{}) bool
	String() string
}

// BaseMatcher ...
type BaseMatcher struct {
	expected interface{}
}

// NewBaseMatcher ...
func NewBaseMatcher(value interface{}) *BaseMatcher {
	matcher := new(BaseMatcher)
	matcher.expected = value
	return matcher
}

// Expected ...
func (matcher *BaseMatcher) Expected() interface{} {
	return matcher.expected
}
