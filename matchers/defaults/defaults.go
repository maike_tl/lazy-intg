package defaults

import "productoffers/common/integration/matchers"

func init() {
	matchers.RegisterMatcher("equal", NewEqual)
	matchers.RegisterMatcher("not_equal", NewNotEqual)
	matchers.RegisterMatcher("is_set", NewIsSet)
	matchers.RegisterMatcher("is_empty", NewIsEmpty)
	matchers.RegisterMatcher("not_set", NewNotSet)
	matchers.RegisterMatcher("match", NewMatch)
	matchers.RegisterMatcher("match_json", NewMatchJSON)
	matchers.RegisterMatcher("json", NewMatchJSONFields)
}
