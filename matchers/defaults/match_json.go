package defaults

import (
	"encoding/json"
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// MatchJSON ...
type MatchJSON struct {
	*matchers.BaseMatcher
}

// NewMatchJSON ...
func NewMatchJSON(values ...interface{}) matchers.IMatcher {
	matcher := new(MatchJSON)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *MatchJSON) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("MatchJSON.Match() expects 1 argument"))
	}

	expected, _ := json.Marshal(matcher.Expected())

	value := string(data[0].([]byte))

	return gomega.Expect(value).To(gomega.MatchJSON(expected))
}

// String ...
func (matcher *MatchJSON) String() string {
	return fmt.Sprintf("match:\n\t%s", matcher.Expected().(map[string]interface{}))
}
