package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// NotSet ...
type NotSet struct {
	*matchers.BaseMatcher
}

// NewNotSet ...
func NewNotSet(values ...interface{}) matchers.IMatcher {
	matcher := new(NotSet)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *NotSet) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("NotSet.Match() expects 1 argument"))
	}

	return gomega.Expect(data[0]).To(gomega.BeNil())
}

// String ...
func (matcher *NotSet) String() string {
	return "not be set"
}
