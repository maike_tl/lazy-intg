package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// IsSet ...
type IsSet struct {
	*matchers.BaseMatcher
}

// NewIsSet ...
func NewIsSet(values ...interface{}) matchers.IMatcher {
	matcher := new(IsSet)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *IsSet) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("IsSet.Match() expects 1 argument"))
	}

	return gomega.Expect(data[0]).ToNot(gomega.BeNil())
}

// String ...
func (matcher *IsSet) String() string {
	return "be set"
}
