package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"
	"regexp"

	gomega "github.com/onsi/gomega"
)

// Match ...
type Match struct {
	*matchers.BaseMatcher
}

// NewMatch ...
func NewMatch(values ...interface{}) matchers.IMatcher {
	matcher := new(Match)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *Match) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("Match.Match() expects 1 argument"))
	}

	if data[0] == nil {
		err := fmt.Errorf("value to be matched cannot be nil")
		return gomega.Expect(err).ToNot(gomega.HaveOccurred())
	}

	regex := matcher.Expected().(string)
	value := data[0].(string)

	return gomega.Expect(regexp.MatchString(regex, value)).To(gomega.BeTrue())
}

// String ...
func (matcher *Match) String() string {
	return fmt.Sprintf("match '%+v'", matcher.Expected())
}
