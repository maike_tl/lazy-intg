package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// Equal ...
type Equal struct {
	*matchers.BaseMatcher
}

// NewEqual ...
func NewEqual(values ...interface{}) matchers.IMatcher {
	matcher := new(Equal)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *Equal) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("Equal.Match() expects 1 argument"))
	}

	return gomega.Expect(data[0]).To(gomega.Equal(matcher.Expected()))
}

// String ...
func (matcher *Equal) String() string {
	return fmt.Sprintf("equal '%+v'", matcher.Expected())
}
