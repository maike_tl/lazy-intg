package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	"github.com/onsi/gomega"

	"encoding/json"
)

// MatchJSONFields ...
type MatchJSONFields struct {
	*matchers.BaseMatcher
	lastError   error
	submatchers map[string]matchers.IMatcher
}

// REVIEW: to make the code below decent, would need to refactor the assertion loader to avoid circular dependencies
func newSubMatcherDefinition(data map[string]interface{}) (matchers.IMatcher, error) {
	var name string
	var value interface{}

	if tmp, found := data["matcher"]; found {
		name = tmp.(string)
	} else {
		return nil, fmt.Errorf("json-fields submatcher definition is invalid: missing [matcher] parameter")
	}
	if tmp, found := data["value"]; found {
		value = tmp
	} else {
		return nil, fmt.Errorf("json-fields submatcher definition is invalid: missing [value] parameter")
	}

	return matchers.New(name, value)
}

// NewMatchJSONFields ...
func NewMatchJSONFields(values ...interface{}) matchers.IMatcher {
	var err error

	matcher := new(MatchJSONFields)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])

	matcher.submatchers = make(map[string]matchers.IMatcher, 0)

	// REVIEW: making this "prettier" would require a refactor of the matchers
	submatchers := matcher.Expected().(map[string]interface{})
	for k, v := range submatchers {
		submatcher, ok := v.(map[string]interface{})
		if !ok {
			panic(fmt.Sprintf("failed to create submatcher"))
		}
		if matcher.submatchers[k], err = newSubMatcherDefinition(submatcher); err != nil {
			panic(err)
		}
	}
	return matcher
}

// Match ...
func (matcher *MatchJSONFields) Match(data ...interface{}) bool {
	var err error
	var value interface{}

	if len(data) != 1 {
		panic(fmt.Errorf("MatchJSONFields.Match() expects 1 argument"))
	}

	// expected := string(matcher.Expected().(map[string]interface{}))

	if err = json.Unmarshal(data[0].([]byte), &value); err != nil {
		return false
	}

	jsondata, found := value.(map[string]interface{})
	if !found {
		return false
	}

	for k, m := range matcher.submatchers {
		if !gomega.Expect(jsondata).To(gomega.HaveKey(k)) {
			return false
		}
		// if _, found := jsondata[k]; !found {
		// 	log.Println("value does not contain field")
		// 	return false
		// }
		if !m.Match(jsondata[k]) {
			return false
		}
	}

	return true
}

// String ...
func (matcher *MatchJSONFields) String() string {
	return fmt.Sprintf("be a JSON response with fields:")
}
