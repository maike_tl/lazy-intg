package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// IsEmpty ...
type IsEmpty struct {
	*matchers.BaseMatcher
}

// NewIsEmpty ...
func NewIsEmpty(values ...interface{}) matchers.IMatcher {
	matcher := new(IsEmpty)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *IsEmpty) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("IsEmpty.Match() expects 1 argument"))
	}

	return gomega.Expect(data[0]).To(gomega.BeEmpty())
}

// String ...
func (matcher *IsEmpty) String() string {
	return "be empty"
}
