package defaults

import (
	"fmt"
	"productoffers/common/integration/matchers"

	gomega "github.com/onsi/gomega"
)

// NotEqual ...
type NotEqual struct {
	*matchers.BaseMatcher
}

// NewNotEqual ...
func NewNotEqual(values ...interface{}) matchers.IMatcher {
	matcher := new(NotEqual)
	matcher.BaseMatcher = matchers.NewBaseMatcher(values[0])
	return matcher
}

// Match ...
func (matcher *NotEqual) Match(data ...interface{}) bool {
	if len(data) != 1 {
		panic(fmt.Errorf("NotEqual.Match() expects 1 argument"))
	}

	return gomega.Expect(data[0]).ToNot(gomega.Equal(matcher.Expected))
}

// String ...
func (matcher *NotEqual) String() string {
	return fmt.Sprintf("not equal '%+v'", matcher.Expected())
}
