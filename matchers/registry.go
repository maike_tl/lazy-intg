package matchers

import "fmt"

var registry *Registry

// Registry ...
type Registry struct {
	matchers map[string]IMatcherFactory
}

func (registry *Registry) getMatcher(name string) IMatcherFactory {
	return getRegistry().matchers[name]
}

func (registry *Registry) setMatcher(name string, matcher IMatcherFactory) *Registry {
	registry.matchers[name] = matcher
	return registry
}

func getRegistry() *Registry {
	if registry == nil {
		registry = new(Registry)
		registry.matchers = make(map[string]IMatcherFactory)
	}

	return registry
}

// New ...
func New(name string, value interface{}) (IMatcher, error) {
	factory := getRegistry().getMatcher(name)

	if factory == nil {
		return nil, fmt.Errorf("matcher '%s' is not registered", name)
	}

	return factory(value), nil
}

// RegisterMatcher ...
func RegisterMatcher(name string, matcher IMatcherFactory) {
	getRegistry().setMatcher(name, matcher)
}
