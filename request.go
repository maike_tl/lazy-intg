package integration

import (
	"bytes"
	"io"
	"net/http"
)

// ScenarioRequest ...
type ScenarioRequest struct {
	description string
	Request
}

// NewScenarioRequest ...
func NewScenarioRequest(
	host string,
	request SpecScenarioRequest,
) *ScenarioRequest {
	r := new(ScenarioRequest)
	r.description = request.Description
	r.Request = *NewRequest(host, request.SpecRequest)

	return r
}

// Request ...
type Request struct {
	method   string
	path     string
	headers  map[string]string
	cookies  map[string]string
	body     []byte
	sendBody bool
}

// NewRequest ...
func NewRequest(
	host string,
	request SpecRequest,
) *Request {
	r := new(Request)
	r.method = request.Method
	r.path = host + request.URI
	r.cookies = request.Cookies
	r.headers = request.Headers

	if len(request.Body) != 0 {
		r.body = []byte(request.Body)
		r.sendBody = true
	}

	return r
}

func (req *Request) isValidResponseStatus(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}

// Do ...
func (req *Request) Do() (*http.Response, error) {
	var reader io.Reader
	client := &http.Client{}

	if req.sendBody {
		reader = bytes.NewReader(req.body)
	}

	httpRequest, err := http.NewRequest(req.method, req.path, reader)

	if err != nil {
		return nil, err
	}

	for key, value := range req.cookies {
		httpRequest.AddCookie(&http.Cookie{Name: key, Value: value})
	}

	for key, value := range req.headers {
		httpRequest.Header.Add(key, value)
	}

	return client.Do(httpRequest)
}
